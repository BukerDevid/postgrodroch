package storage

import (
	"context"
	"errors"
	"log"
	"sync"

	pgx "github.com/jackc/pgx/v4"
	pgxp "github.com/jackc/pgx/v4/pgxpool"
)

type dbTypeDo uint8

type DoQuery struct {
	do    dbTypeDo
	ctx   context.Context
	query string
	args  any

	Done     chan struct{}
	ReadDone chan struct{}

	ResRows pgx.Rows
	ResRow  pgx.Row
	Scan    interface{}
	Err     error
}

var (
	onceInit sync.Once
	onceRun  sync.Once
	provider *pgxp.Pool
	bc       *balancer
)

const (
	DBTypeDoExec dbTypeDo = iota
	DBTypeDoQueryRow
	DBTypeDoQuery
)

// NOTE: using init for set once
func init() {
	onceInit = sync.Once{}
	onceRun = sync.Once{}
}

func NewQuery(qType dbTypeDo, ctx context.Context, query string, args any) *DoQuery {
	return &DoQuery{
		do:       qType,
		ctx:      ctx,
		query:    query,
		args:     args,
		Done:     make(chan struct{}),
		ReadDone: make(chan struct{}),
	}
}

func NewQueryWithScan(qType dbTypeDo, ctx context.Context, query string, args any, scan any) *DoQuery {
	return &DoQuery{
		do:       qType,
		ctx:      ctx,
		query:    query,
		args:     args,
		Done:     make(chan struct{}),
		ReadDone: make(chan struct{}),
		Scan:     scan,
	}
}

func InWork(query *DoQuery, idx int) {
	bc.NewWork(query)
}

func (d *DoQuery) Do(dbConn *pgxp.Conn) {
	defer func() {
		d.Done <- struct{}{}
	}()

	switch d.do {
	case DBTypeDoExec:
		{
			_, d.Err = dbConn.Exec(d.ctx, d.query, d.args.([]interface{})...)
			break
		}
	case DBTypeDoQuery:
		{
			d.ResRows, d.Err = dbConn.Query(d.ctx, d.query, d.args.([]interface{})...)
			break
		}
	case DBTypeDoQueryRow:
		{
			// d.ResRow = dbConn.QueryRow(d.ctx, d.query, d.args.([]interface{})...)
			_, d.Err = dbConn.QueryFunc(d.ctx, d.query, d.args.([]interface{}), d.Scan.([]interface{}), func(qfr pgx.QueryFuncRow) error {
				return nil
			})
			break
		}
	}
}

func SetProvider(db *pgxp.Pool, log log.Logger, workers uint16, kill chan struct{}) (chan struct{}, error) {
	onceInit.Do(func() {
		provider = db

		if workers < 10 {
			workers = 10
		}

		log.Println("Init balancer")
		bc = &balancer{
			worker:   workers,
			log:      log,
			workChan: make(chan *DoQuery, workers),
			kill:     kill,
			done:     make(chan struct{}),
		}

	})

	if bc == nil || provider == nil {
		return nil, errors.New("not init providers")
	}

	RunWorkers()
	return bc.done, nil
}

type balancer struct {
	worker uint16
	kill   chan struct{}
	done   chan struct{}

	wg sync.WaitGroup

	log      log.Logger
	workChan chan *DoQuery
}

func RunWorkers() {
	onceRun.Do(func() {
		bc.run()
	})
}

func (b *balancer) run() error {
	for idx := uint16(0); idx < b.worker; idx++ {
		b.wg.Add(1)
		go b.WorkerStorage()
	}

	go func(wg *sync.WaitGroup, done chan struct{}) {
		b.wg.Wait()
		b.log.Print("Storage stop all worker")
		done <- struct{}{}
	}(&b.wg, b.done)

	return nil
}

func (b *balancer) WorkerStorage() {
	defer func(wg *sync.WaitGroup) {
		wg.Done()
		log.Println("[workerStorage] done")
	}(&b.wg)

CONN_REWRITE:
	conn, err := provider.Acquire(context.Background())
	if err != nil {
		log.Printf("[workerStorage] can't get connect - %v", err)
		return
	}

	for idx := uint8(0); idx < 10; idx++ {
		select {
		case query, ok := <-b.workChan:
			if ok {
				query.Do(conn)
			} else {
				conn.Release()
				log.Println("[workerStorage] chan work close")
				return
			}
		case _, ok := <-b.kill:
			if !ok {
				conn.Release()
				return
			}
		}
	}
	conn.Release()
	goto CONN_REWRITE
}

func (b *balancer) NewWork(query *DoQuery) {
	select {
	case _, ok := <-b.kill:
		if !ok {
			query.Err = errors.New("stop workers")
			query.Done <- struct{}{}
			return
		}
	default:
	}

	b.workChan <- query
}
