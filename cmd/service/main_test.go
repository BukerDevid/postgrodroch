package main_test

import (
	"context"
	"errors"
	"log"
	"os"
	"strconv"
	"sync"
	"testing"
	"testpg/internal/storage"
	"testpg/pkg/database"
	"testpg/pkg/utility"
)

var kill chan struct{} = make(chan struct{})
var done chan struct{}

func TestMain(m *testing.M) {
	os.Setenv(utility.ENV_DriverConfig, "yaml")
	os.Setenv(utility.ENV_PathConfig, "../../configs/config.yaml")

	// INFO: read configuration
	config, err := utility.ReadConfig()
	if err != nil {
		log.Fatalf("Invalid read config %v", err)
	}

	// INFO: init provider
	provider, err := database.NewProvider(
		config.Database.Host,
		config.Database.Port,
		config.Database.User,
		config.Database.Password,
		config.Database.Database,
		config.Database.Pool,
	)
	if err != nil {
		log.Fatalf("Invalid init provider %v", err)
	}

	// INFO: set database provider and running workers
	if done, err = storage.SetProvider(provider, *log.Default(), uint16(config.Database.Pool), kill); err != nil {
		log.Fatalf("Invalid init provider %v", err)
	}

	log.Println("Main ready")
	m.Run()
}

type RowQuery struct {
	Exec      string
	Query     string
	ExecArgs  any
	QueryArgs any
}

func adapterArgs(args ...any) []any {
	return args
}

func initQuery(limit int) ([]RowQuery, error) {
	if limit < 1 || limit > 10_000_000 {
		return nil, errors.New("invalid limit querys; >= 1 && <= 10_000_000")
	}

	var tQuerys []RowQuery = make([]RowQuery, limit)
	for idx := 0; idx < limit; idx++ {
		tQuerys[idx].Exec = `
		INSERT INTO 
			"data_heap"(
				"id", 
				"payload"
			) VALUES ($1,$2)
		`
		tQuerys[idx].ExecArgs = adapterArgs(idx, "payload-"+strconv.Itoa(idx))

		tQuerys[idx].Query = `
		SELECT 
			"id"
		FROM
			"data_heap"
		WHERE
			"id" = $1
		`

		tQuerys[idx].QueryArgs = adapterArgs(idx)
	}

	return tQuerys, nil
}

func BenchmarkWorker(b *testing.B) {
	tQuery1, err := initQuery(1_000_000)
	if err != nil {
		b.Fatal(err)
	}

	wg := sync.WaitGroup{}

	// t.Log("Running and ready for get")

	for idx, query := range tQuery1 {
		query := query

		wg.Add(1)

		go func(idx int, query *RowQuery, wg *sync.WaitGroup) {
			// NOTE: If need use time limit
			// ctx, cCancel := context.WithTimeout(context.TODO(), time.Second*170)
			ctx := context.TODO()
			defer func() {
				wg.Done()
				// NOTE: If need use time limit
				// cCancel()
			}()

			// INFO: Create note in DB
			queryExec := storage.NewQuery(storage.DBTypeDoExec, ctx, query.Exec, query.ExecArgs)
			storage.InWork(queryExec, idx)
			<-queryExec.Done

			if queryExec.Err != nil {
				log.Printf("Failed query %v", queryExec.Err)
				return
			}

			var id int
			scan := adapterArgs(&id)
			queryQuery := storage.NewQueryWithScan(storage.DBTypeDoQueryRow, ctx, query.Query, query.QueryArgs, scan)
			storage.InWork(queryQuery, idx)
			<-queryQuery.Done

			if queryQuery.Err != nil {
				b.Error("Query invalid", err)
				return
			}

			if id == idx {
				return
			}

			b.Errorf("invalid equal %d idx with scan %d", idx, id)
		}(idx, &query, &wg)
	}

	wg.Wait()
	// close(kill)
	close(kill)

	<-done
}

func TestWorker(t *testing.T) {
	tQuery1, err := initQuery(1_000_000)
	if err != nil {
		t.Fatal(err)
	}

	wg := sync.WaitGroup{}

	// t.Log("Running and ready for get")

	for idx, query := range tQuery1 {
		query := query

		wg.Add(1)

		go func(idx int, query *RowQuery, wg *sync.WaitGroup) {
			// NOTE: If need use time limit
			// ctx, cCancel := context.WithTimeout(context.TODO(), time.Second*170)
			ctx := context.TODO()
			defer func() {
				wg.Done()
				// NOTE: If need use time limit
				// cCancel()
			}()

			// INFO: Create note in DB
			queryExec := storage.NewQuery(storage.DBTypeDoExec, ctx, query.Exec, query.ExecArgs)
			storage.InWork(queryExec, idx)
			<-queryExec.Done

			if queryExec.Err != nil {
				log.Printf("Failed query %v", queryExec.Err)
				return
			}

			var id int
			scan := adapterArgs(&id)
			queryQuery := storage.NewQueryWithScan(storage.DBTypeDoQueryRow, ctx, query.Query, query.QueryArgs, scan)
			storage.InWork(queryQuery, idx)
			<-queryQuery.Done

			if queryQuery.Err != nil {
				t.Error("Query invalid", err)
				return
			}

			if id == idx {
				return
			}

			t.Errorf("invalid equal %d idx with scan %d", idx, id)
		}(idx, &query, &wg)
	}

	wg.Wait()
	// close(kill)
	close(kill)

	<-done
}
